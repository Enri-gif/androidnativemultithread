#include <stdio.h>
#include <jni.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include "inupt.c"


JNIEXPORT jlong JNICALL
Java_com_example_inputtest_MainActivity_stringFromJNI(JNIEnv *env, jobject thiz, jint i) {
    long int result;
    result = returnValue((int)i);

    return result;
}