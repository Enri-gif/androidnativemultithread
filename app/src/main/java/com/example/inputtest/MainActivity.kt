package com.example.inputtest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.inputtest.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)



        binding.button.setOnClickListener{
            val edit = binding.input.text.toString()
            val s = stringFromJNI(edit.toInt())
            // Example of a call to a native method
            binding.result.text = s.toString()
        }
    }

    /**
     * A native method that is implemented by the 'inputtest' native library,
     * which is packaged with this application.
    */
    external fun stringFromJNI(int: Int): Long

    companion object {
        // Used to load the 'inputtest' library on application startup.
        init {
            System.loadLibrary("com.c")
        }
    }
}